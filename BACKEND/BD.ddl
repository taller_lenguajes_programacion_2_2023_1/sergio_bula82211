-- Generado por Oracle SQL Developer Data Modeler 22.2.0.165.1149
--   en:        2023-02-22 11:12:32 COT
--   sitio:      Oracle Database 11g
--   tipo:      Oracle Database 11g



-- predefined type, no DDL - MDSYS.SDO_GEOMETRY

-- predefined type, no DDL - XMLTYPE

CREATE TABLE cliente (
    id_cliente  VARCHAR2(5) NOT NULL,
    direccion   VARCHAR2(30) NOT NULL,
    metodo_pago BLOB NOT NULL,
    id_persona  VARCHAR2(5) NOT NULL
);

ALTER TABLE cliente ADD CONSTRAINT cliente_pk PRIMARY KEY ( id_cliente );

CREATE TABLE detalle_pedido (
    id_detalle  NUMBER NOT NULL,
    cantidad    NUMBER NOT NULL,
    precio      NUMBER NOT NULL,
    id_pedido   NUMBER NOT NULL,
    id_producto VARCHAR2(5) NOT NULL
);

ALTER TABLE detalle_pedido ADD CONSTRAINT detalle_pedido_pk PRIMARY KEY ( id_detalle );

CREATE TABLE pedido (
    id_pedido         NUMBER NOT NULL,
    fecha             DATE NOT NULL,
    id_transportadora VARCHAR2(5) NOT NULL,
    id_cliente        VARCHAR2(5) NOT NULL
);

ALTER TABLE pedido ADD CONSTRAINT pedido_pk PRIMARY KEY ( id_pedido );

CREATE TABLE persona (
    id_persona VARCHAR2(5) NOT NULL,
    nombre     VARCHAR2(30) NOT NULL,
    apellido   VARCHAR2(30) NOT NULL,
    telefono   VARCHAR2(10)
);

ALTER TABLE persona ADD CONSTRAINT persona_pk PRIMARY KEY ( id_persona );

CREATE TABLE producto (
    id_producto VARCHAR2(5) NOT NULL,
    precio      NUMBER NOT NULL,
    tipo        VARCHAR2(15) NOT NULL,
    descripcion VARCHAR2(200) NOT NULL,
    stock       NUMBER NOT NULL
);

ALTER TABLE producto ADD CONSTRAINT producto_pk PRIMARY KEY ( id_producto );

CREATE TABLE tarjeta (
    id_tarjeta    NUMBER NOT NULL,
    numero        NUMBER NOT NULL,
    f_vencimiento DATE NOT NULL,
    cod           NUMBER NOT NULL,
    id_cliente    VARCHAR2(5) NOT NULL
);

ALTER TABLE tarjeta ADD CONSTRAINT tarjeta_pk PRIMARY KEY ( id_tarjeta );

CREATE TABLE transportadora (
    id_transportadora VARCHAR2(5) NOT NULL,
    id_persona        VARCHAR2(5) NOT NULL
);

ALTER TABLE transportadora ADD CONSTRAINT transportadora_pk PRIMARY KEY ( id_transportadora );

ALTER TABLE cliente
    ADD CONSTRAINT cliente_persona_fk FOREIGN KEY ( id_persona )
        REFERENCES persona ( id_persona );

ALTER TABLE detalle_pedido
    ADD CONSTRAINT detalle_pedido_pedido_fk FOREIGN KEY ( id_pedido )
        REFERENCES pedido ( id_pedido );

ALTER TABLE detalle_pedido
    ADD CONSTRAINT detalle_pedido_producto_fk FOREIGN KEY ( id_producto )
        REFERENCES producto ( id_producto );

ALTER TABLE pedido
    ADD CONSTRAINT pedido_cliente_fk FOREIGN KEY ( id_cliente )
        REFERENCES cliente ( id_cliente );

ALTER TABLE pedido
    ADD CONSTRAINT pedido_transportadora_fk FOREIGN KEY ( id_transportadora )
        REFERENCES transportadora ( id_transportadora );

ALTER TABLE tarjeta
    ADD CONSTRAINT tarjeta_cliente_fk FOREIGN KEY ( id_cliente )
        REFERENCES cliente ( id_cliente );

ALTER TABLE transportadora
    ADD CONSTRAINT transportadora_persona_fk FOREIGN KEY ( id_persona )
        REFERENCES persona ( id_persona );



-- Informe de Resumen de Oracle SQL Developer Data Modeler: 
-- 
-- CREATE TABLE                             7
-- CREATE INDEX                             0
-- ALTER TABLE                             14
-- CREATE VIEW                              0
-- ALTER VIEW                               0
-- CREATE PACKAGE                           0
-- CREATE PACKAGE BODY                      0
-- CREATE PROCEDURE                         0
-- CREATE FUNCTION                          0
-- CREATE TRIGGER                           0
-- ALTER TRIGGER                            0
-- CREATE COLLECTION TYPE                   0
-- CREATE STRUCTURED TYPE                   0
-- CREATE STRUCTURED TYPE BODY              0
-- CREATE CLUSTER                           0
-- CREATE CONTEXT                           0
-- CREATE DATABASE                          0
-- CREATE DIMENSION                         0
-- CREATE DIRECTORY                         0
-- CREATE DISK GROUP                        0
-- CREATE ROLE                              0
-- CREATE ROLLBACK SEGMENT                  0
-- CREATE SEQUENCE                          0
-- CREATE MATERIALIZED VIEW                 0
-- CREATE MATERIALIZED VIEW LOG             0
-- CREATE SYNONYM                           0
-- CREATE TABLESPACE                        0
-- CREATE USER                              0
-- 
-- DROP TABLESPACE                          0
-- DROP DATABASE                            0
-- 
-- REDACTION POLICY                         0
-- 
-- ORDS DROP SCHEMA                         0
-- ORDS ENABLE SCHEMA                       0
-- ORDS ENABLE OBJECT                       0
-- 
-- ERRORS                                   0
-- WARNINGS                                 0
